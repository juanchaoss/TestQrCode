<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Pagina Principal</title>
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/bootstrap-theme.css">
</head>
<body>
<h3 class="text-danger text-center">SISTEMA DEMO</h3>
<h4 class="text-center"><small>Solo existen los ID 1 y 2</small></h4>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-4">
			<div class="input-group">
		      <input type="text" class="form-control" id="input" placeholder="ID-PERSONA">
		      <span class="input-group-btn">
		        <button id="btn_search" class="btn btn-primary" type="button">Buscar!</span></button>
		      </span>
		    </div><!-- /input-group -->
		    <br>
		    <div class="alert alert-success" id="msj_good" role="alert">EXITO...!! Persona Encontrada</div>
		    <div class="alert alert-danger" id="msj_error" role="alert">ERROR... El campo esta vacio...!</div>
		    <div class="alert alert-warning" id="msj_exist" role="alert">ERROR... El ID del empleado no existe</div>
		</div>
		<div class="col-lg-4" id="qr"></div>
		<div class="col-lg-4">
			<button id="print" class="btn btn-success">Imprimir!</button>
		</div>
	</div>
</div>
<script src="../js/jquery-1.12.0.js"></script>
<script src="../js/jquery.qrcode-0.12.0.js"></script>
<script src="../controlador/js/controller.js"></script>
<script>
	//listiner de eventos
	$(document).on('ready',function(){
		//declaracion del evento click
		$('#btn_search').click(function() {
			/* Act on the event */
			//llamado de la function 
			getSomething();
		});
		//ocultar la capa de msj de error
		$('#msj_error').hide();
		//ocultar la capa de msj good
		$("#msj_good").hide();
		//ocultar la capa de msj exist
		$('#msj_exist').hide();

		$("#print").click(function(){
			window.open('lolo.php').print();
		});

		$("#test").click(function(){
			testSelect();
		});
	});
</script>
</body>
</html>