<?php 
	//inserto la clase ExcuteQuery
	include_once("clases/ExecuteQuery.php");

	//creacion de la Clase Something

	class Sesion{
		//prototipo del metodo
		public function iniciarSesion($usuario,$clave){
			//instancia de clase ExcuteQuery
			$bd = new ExecuteQuery();
			//instancia del metodo abrir()
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			$final_user = pg_escape_string($usuario);
			$final_pass = pg_escape_string($clave);

			$sql = "SELECT * FROM usuario WHERE usuario = '$final_user' AND clave = '$final_pass' AND eliminado_usu = 0";
			$output = $bd->consultar($sql, 'ARREGLO');

			if ($output) {
				session_start();
				$_SESSION['usuario'] = $output[0]['usuario'];
				$_SESSION['permitido'] = TRUE;
				return TRUE;
			}else
				return FALSE;
		}

		public function cerrarSesion(){
			session_start();
			session_destroy();
			session_unset();
			return TRUE;
		}
	}
?>