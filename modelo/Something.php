<?php 
	//inserto la clase ExcuteQuery
	include_once("clases/ExecuteQuery.php");

	//creacion de la Clase Something

	class Something{
		//prototipo del metodo
		public function getSomething($input){

			//instancia de clase ExcuteQuery
			$bd = new ExecuteQuery();
			//instancia del metodo abrir()
			$bd->abrir(BD, SERVIDOR, USUARIO, CLAVE, PUERTO);

			//preapracion de la consulta sql
			$sql="SELECT * FROM persona AS P JOIN usuario as u ON(p.id_per = u.id_per) WHERE p.id_per = $input";
			//instancio el metodo consultar para ejecutar la consulta
			$output = $bd->consultar($sql, 'ARREGLO');
			//retorno un valor booleano 1 = cierto 0 = falso
			return $output;
		}
	}
?>