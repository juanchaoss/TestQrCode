
/*##########LISTENER - CALLBACKS##########*/
$(document).on('ready',function(){
	$("#alert_empty").hide();
	$("#alert_notExis").hide();

	$("#sesion_sesion").click(function(){
		iniciarSesion();
	});
});

/*####### FUNCTIONS ########*/

function iniciarSesion(){
	var usuario = $("#user").val();
	var clave   = $("#pass").val();

	if ($.trim(usuario) == '' && $.trim(clave) == '') {
		
		$("#alert_empty").show().fadeOut(6500);
		return;
	}else if($.trim(usuario) == ''){
		$("#alert_empty").show().fadeOut(4000);
		$("#user").focus();
	}else if($.trim(clave) == ''){
		$("#alert_empty").show().fadeOut(4000);
		$("#pass").focus();
	}else{
		$.ajax( {
			type : 'POST',
			url : "controlador/trans/tSesion.php", // <--- ruta a donde se enviaran los datos
			data :	{accion:'iniciarSesion',usuario:usuario,clave:clave},//  <--- se prapara la informacion a enviar en forma de json
			//dataType: 'json',
			error : function(data) {
				alert("Ups... Algo esta mal :(  ----> iniciarSesion");
			},
			success : function(data) {
				//atrapo el json en el resp
				var resp = $.parseJSON(data);
				//validacion booleana
				if (resp) {
					cambiarRutaSesionActiva();
				}else{
					
				}
			}
	    });
	}
}

function cerrarSesion(){

}

function limpiarFormUser(){
	$("#user").val('');
	$("#pass").val('');
}

function cambiarRutaSesionActiva(){
	location.href="vista/index.php";
}

function cambiarRutaSesionCerrada(){
	location.href="../";
}

function isSesionActiva(){

}