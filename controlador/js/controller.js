//prototipo de la funcion 
function getSomething(){
	var input = $("#input").val();	

	if ($.trim(input) == '') { //verifico que haya algun valor en el campo
		$('#msj_error').show();
		$('#msj_error').fadeOut(2300);
		return;
	}else{
		//llamado al metodo ajax
		$.ajax( {
			type : 'POST',
			url : "../controlador/trans/tSomething.php", // <--- ruta a donde se enviaran los datos
			data :	{accion:'getSomething',input:input},//  <--- se prapara la informacion a enviar en forma de json
			//dataType: 'json',
			error : function(data) {
				alert("Ups... Algo esta mal :(");
			},
			success : function(data) {
				//atrapo el json en el resp
				var resp = $.parseJSON(data);
				//validacion booleana
				if (resp) {
					//limpio la capa con id qr
					$('#qr').html('');
					//muestro el mensaje
					$('#msj_good').show();
					//creo una variable con el msj que va almacenar el codigo qr
					var msj_final = "Persona de la base de datos ==> Nombre ==>"+resp[0]['nombre']+
									" Apellido==> "+resp[0]['apellido']+" Usuario de sistema ==>"+resp[0]['usuario'];
					//mando imprimir por consola el msj
					console.log(msj_final);
					//genero el codigo qr, la variable msj_final almacena la informacion
					$('#qr').qrcode({
						render:'divs',
						size:100,
						color:'#3A3',
						text:msj_final
					});
				}else{
					//esto es en caso de que haya un error de tipo que no exista el id
					$('#msj_good').hide();
					$('#msj_error').hide();
					$('#msj_exist').show();
					$("#qr").empty();
					$('#msj_exist').fadeOut(2300);
				}
			}
	    });
	}
}
